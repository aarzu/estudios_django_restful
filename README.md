# estudios_django_restful

Del curso RestFul Api de Udemy

Hemos visto 4 tipos de vistas

Vistas Django Puro

Sub Clases APIView

Sub Clases generics.*

viewsets.ModelViewSet

Así que cómo o cuándo usar cual vista.  Aunque no es ley, sólo planteo posibilidades que considero más adecuadas.

Use viewsets.ModelViewSet cuando va a permitir todas o la mayoría de las operaciones CRUD en un modelo.

Use genéricos.* Cuando solo desee permitir algunas operaciones en un modelo

Usa APIView cuando quieras personalizar completamente el comportamiento